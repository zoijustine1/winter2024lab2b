import java.util.Scanner;
public class Hangman{
	
	public static int isLetterInWord(String word, char letterOfTheWord) {
	
		//performs a while loop to get every characters in word.
		for (int i = 0; i<4 ; i++){
		//Gets each characters of word.
		// return the number corresponding to the letter.
			if (word.charAt(i)== letterOfTheWord){
				return i;
			}
			
		}
		return -1;
	}	
	public static char toUpperCase(char charactersOfTheWord) {
		//converting the word into upperCase
		charactersOfTheWord = Character.toUpperCase(charactersOfTheWord);
		// returning the word in upperCase
		return charactersOfTheWord;
	}
	public static void printWork(String word, boolean[] letters) {
		String newWord = "";
		for(int i = 0; i < word.length();i++){ 
			if (letters[i]){
				// the letter replaces the space
				newWord= newWord+ word.charAt(i);
			}
			else {
				//leaves it as a blank if false
				newWord = newWord+" _ ";
			}		
		}
		//printing the result of every iteration
		System.out.println("Your result is "+ newWord);
	}
	public static void runGame(String word){
		boolean[] letters = {false,false,false,false}; 
		int numberOfGuess = 0;
		
		while(numberOfGuess < 6 && !(letters[0] && letters[1] && letters[2] && letters[3])){
			//Gets input from user
			Scanner reader = new Scanner (System.in);
			//Printing that the letter should give a letter
			System.out.println("Guess a letter");
			//getting the letter of the word provided by the user.
			char guess = reader.nextLine().charAt(0);
			//converting the words to upper case letters
			guess =toUpperCase(guess);
			
			for(int i = 0; i < letters.length;i++){
				if(isLetterInWord(word, guess)== i){
					letters[i] = true;
				}
			}
			if(isLetterInWord(word, guess)== -1){
				numberOfGuess++;
			}
			//calling the printWord methods and 
			//passing the values of word and guess
			printWork(word, letters);
		}
		if (numberOfGuess==6){
			System.out.println("Oops! Better luck next time :)" );
		}
		else {
			System.out.println("Congrats! You got it :)");
		}
	}
}