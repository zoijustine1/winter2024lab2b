import java.util.Scanner;

public class GameLauncher {
	/*
	The main method calls the chooseGame method to start the game.
	*/
	public static void main(String[] args){
		chooseGame();
	
	}
	/*This method gets a word from the user and converting it to upperCase letters
	*It also calls the runGame method from the Hangman class or Hangman java file and taking
	*the word that the user entered as the word to be guessed.
	*/
	public static void runHangman(){
		Scanner reader = new Scanner (System.in);
		//Get user input
		System.out.println("Enter a 4-letter word:");
		String word = reader.next();
		//Convert to upper case
		word = word.toUpperCase();
		
		//Start hangman game
		Hangman.runGame(word);	
	}
	/* This method generates word from the class Wordle and run the runGame method 
	* to start the game located in the Wordle class.
	*/
	public static void runWordle(){
		String answer = Wordle.generateWord();
		Wordle.runGame(answer);
	}
	
	/*This method asks the user to press 1 or 2 to start either Hangman or Worldle
	* if the user didn't choose a game(1 or 2), it will loop to ask the user to choose a game
	*if the user press 1, it will call the method runHangman and start the Hangman game
	* if the user press 2, it will call the method runWordle to start the Wordle game.
	*/
	public static void chooseGame(){
		System.out.println("Hello Player! Press 1 to play Hangman. Press 2 to play Wordle");
		Scanner reader = new Scanner(System.in);
		final int NUM_TO_RUN_HANGMAN = 1;
		final int NUM_TO_RUN_WORDLE = 2;
		int userInput = reader.nextInt();
		while(!(userInput == NUM_TO_RUN_HANGMAN || userInput == NUM_TO_RUN_WORDLE)){
			System.out.println("You didn't choose either of the game! Press 1 to play Hangman. Press 2 to play Wordle");
			userInput = reader.nextInt();
		}
		if(userInput == NUM_TO_RUN_HANGMAN){
			runHangman();
		}
		else {
			runWordle();
		}
	}
}